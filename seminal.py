#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  seminal.py
#  Copyright 2017 Kevin Cole <ubuntourist@hacdc.org> 2017.11.18
#
#  Scanning Electron Microscope interface
#
#    * Press [Enter] to begin logging.
#    * Press Ctrl-C  to close the log and exit.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#


import os
import sys
import readline                 # Command line history
import atexit                   # Interpreter exit handlers
from   Gauge import MKS925

__appname__    = "SEMinal"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"
__module__     = ""


def helper():
    """List available commands"""
    commands  = "help        - List available commands\n"
    commands += "connect     - Connect to vacuum gauge\n"
    commands += "disconnect  - Disconnect from vacuum gauge\n"
    commands += "log         - Log vacuum gauge pressure 10x/sec to screen and file\n"
    commands += "quit        - Quit this program"
    print(commands)
    return


def main():
    _ = os.system("clear")
    print("{0} v.{1}\n{2} ({3})\n{4}, {5} <{6}>\n"
          .format(__appname__,
                  __version__,
                  __copyright__,
                  __license__,
                  __author__,
                  __agency__,
                  __email__))

    # Set up persistent command line history
    #
    histfile = os.path.join(os.path.expanduser("~"), ".seminal_history")
    if not os.path.isfile(histfile):      # If there's no history file...
        empty = open(histfile, "a")       # ... create an ALMOST empty one...
        empty.write("_HiStOrY_V2_\n")     # ... with the special header line
        empty.close()
    readline.read_history_file(histfile)  # Read history from old sessions
    readline.set_history_length(1000)     # Default length was -1 (infinite)

    atexit.register(readline.write_history_file,
                    histfile)             # Save history on exit
    gauge = MKS925.MKS925()
    commands = {"help":       helper,
                "connect":    gauge.connect,
                "disconnect": gauge.disconnect,
                "log":        gauge.log,
                "quit":       gauge.quit}

    while True:
        command = input("> ")
        if command in commands:
            commands[command]()

    return 0


if __name__ == "__main__":
    main()
